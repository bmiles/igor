class AddVisibilityToSamples < ActiveRecord::Migration
  def change
    add_column :samples, :visibility, :symbol
  end
end
