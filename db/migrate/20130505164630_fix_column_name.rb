class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :samples, :type, :variety
  end
end
