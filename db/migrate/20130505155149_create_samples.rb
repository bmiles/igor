class CreateSamples < ActiveRecord::Migration
  def change
    create_table :samples do |t|
      t.string :name
      t.string :type
      t.text :description
      t.string :location
      t.string :creator

      t.timestamps
    end
  end
end
