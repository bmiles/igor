class Sample < ActiveRecord::Base
  attr_accessible :creator, :description, :location, :name, :variety, :visibility
  belongs_to :user
end
