# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sample do
    name "MyString"
    type ""
    description "MyText"
    location "MyString"
    creator "MyString"
  end
end
