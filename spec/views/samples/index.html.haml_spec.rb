require 'spec_helper'

describe "samples/index" do
  before(:each) do
    assign(:samples, [
      stub_model(Sample,
        :name => "Name",
        :type => "Type",
        :description => "MyText",
        :location => "Location",
        :creator => "Creator"
      ),
      stub_model(Sample,
        :name => "Name",
        :type => "Type",
        :description => "MyText",
        :location => "Location",
        :creator => "Creator"
      )
    ])
  end

  it "renders a list of samples" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Type".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Location".to_s, :count => 2
    assert_select "tr>td", :text => "Creator".to_s, :count => 2
  end
end
