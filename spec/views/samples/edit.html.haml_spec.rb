require 'spec_helper'

describe "samples/edit" do
  before(:each) do
    @sample = assign(:sample, stub_model(Sample,
      :name => "MyString",
      :type => "",
      :description => "MyText",
      :location => "MyString",
      :creator => "MyString"
    ))
  end

  it "renders the edit sample form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", sample_path(@sample), "post" do
      assert_select "input#sample_name[name=?]", "sample[name]"
      assert_select "input#sample_type[name=?]", "sample[type]"
      assert_select "textarea#sample_description[name=?]", "sample[description]"
      assert_select "input#sample_location[name=?]", "sample[location]"
      assert_select "input#sample_creator[name=?]", "sample[creator]"
    end
  end
end
